# HISTORY OF FREEZING AND THAWING OF LAKE MENDOTA, 1852-53 to 2017-18
### Wisconsin State Climatology Office
### http://www.aos.wisc.edu/~sco/lakes/Mendota-ice.html


## Monthly Dataviz Battle -- /r/dataisbeautiful
https://www.reddit.com/r/dataisbeautiful/comments/a2p5f0/battle_dataviz_battle_for_the_month_of_december/


![](median.png)

## Code
```r
library(tidyverse)
library(lubridate)

# Load and process data

mendota <- read_csv("../data/lake_mendota_history.csv")

mendota$WINTER <- as.integer(str_trunc(mendota$WINTER,width = 4,ellipsis = ""))

mendota$CLOSED <- str_replace(mendota$CLOSED, " ", "-")
mendota$CLOSED <- if_else(str_extract(mendota$CLOSED, "([A-Z][a-z]+)") %in% c("Jan","Mar"), ydm(paste0(mendota$WINTER + 1,"-",mendota$CLOSED)), ydm(paste0(mendota$WINTER,"-",mendota$CLOSED)))

mendota$OPENED <- str_replace(mendota$OPENED, " ", "-")
mendota$OPENED <- ydm(paste0(mendota$WINTER + 1,"-",mendota$OPENED))

mendota$DAYS <- as.integer(mendota$DAYS)


# Build simple line chart aggregating by decade

mendota %>%
  group_by(decade = 10 * (WINTER %/% 10)) %>%
  mutate(median_days = median(DAYS, na.rm = T)) %>%
  filter(decade >= 1860) %>%
  ggplot(aes(decade, median_days)) +
  geom_line(size = 2, col = "slategrey") +
  geom_point(shape=21, fill="white", size = 4, col = "slategrey") +
  expand_limits(y=60) +
  scale_x_continuous(breaks = seq(1860, 2010, 20)) +
  labs(x = "", y = "Number of Days Closed",
       title = str_to_title("HISTORY OF FREEZING AND THAWING OF LAKE MENDOTA"),
       subtitle = "Median Number of Days Closed By Decade",
       caption = "By Dave Bloom | @DaveBloom11\nSource: Wisconsin State Climatology Office")


```
